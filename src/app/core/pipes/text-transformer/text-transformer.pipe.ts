import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CardSymbols } from '../../helpers/symbols';

const symbols = CardSymbols;

@Pipe({
  name: 'ttp'
})
export class TextTransformerPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(value: any, ...args: any[]): any {
    const type = args[0];
    if (type && value) {
      switch (type) {
        case 'text':
          value = this.transformText(value);
          break;
        default:
          break;
      }
    }

    return value;
  }

  transformText(text: string) {
    text = this.replaceCommon(text);

    text = this.italisizeText(text);

    text = this.replaceWithSymbol(text);

    return text;
  }

  replaceWithSymbol(text: string) {
    const regex = /\{[\d\w]}/g;
    const betweenPara = text.match(regex);

    if (betweenPara) {
      betweenPara.forEach(d => {
        const symbol = symbols.find(dd => dd.symbol === d);
        if (symbol) {
          const temp = `<img class="symbol-small" src="${symbol.svg_uri}">`;
          text = text.replace(d, temp);
        }
      });
    }

    return text;
  }

  italisizeText(text: string) {
    const regex = /\(([^)]+)\)/g;

    const betweenPara = text.match(regex);
    if (betweenPara) {
      betweenPara.forEach(d => {
        const temp = `<i>${d}</i>`;
        text = text.replace(d, temp);
      });
    }

    return text;
  }

  replaceCommon(text: string) {
    const symbol = ' X ';
    text = text.replace(
      symbol,
      ` <img class="symbol-small" src="https://img.scryfall.com/symbology/X.svg"> `
    );

    return text;
  }
}
