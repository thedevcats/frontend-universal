import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardsDashboardComponent } from './components/cards-dashboard/cards-dashboard.component';
import { CardItemComponent } from './components/card-item/card-item.component';
import { CardDetailsComponent } from './components/card-details/card-details.component';
import { NG_ENTITY_SERVICE_CONFIG } from '@datorama/akita-ng-entity-service';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { FakeBackendProvider } from './core/interceptors/fake-backend.interceptor';
import { TextTransformerPipe } from './core/pipes/text-transformer/text-transformer.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CardsDashboardComponent,
    CardItemComponent,
    CardDetailsComponent,
    TextTransformerPipe
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    HttpClientModule,
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    AkitaNgRouterStoreModule.forRoot()
  ],
  providers: [
    FakeBackendProvider,
    {
      provide: NG_ENTITY_SERVICE_CONFIG,
      useValue: { baseUrl: 'https://jsonplaceholder.typicode.com' }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
