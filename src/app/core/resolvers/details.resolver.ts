import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { CoreService } from '../store/core.service';

@Injectable({
  providedIn: 'root'
})
export class DetailsResolver implements Resolve<any> {
  constructor(private coreService: CoreService) {}

  resolve(route: ActivatedRouteSnapshot) {
    const id = route.params.id;

    this.coreService.getCardById(id);
  }
}
