import { Component } from '@angular/core';
import { AppService } from 'src/app/core/services/app/app.service';

@Component({
  selector: 'app-cards-dashboard',
  templateUrl: './cards-dashboard.component.html',
  styleUrls: ['./cards-dashboard.component.scss']
})
export class CardsDashboardComponent {
  cards$ = this.appService.cards$;

  constructor(private appService: AppService) {}

  navigateToDetails(id: string) {
    this.appService.navigateToDetails(id);
  }

  trackByFn(__, item) {
    return item.id;
  }
}
