import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getAllCards() {
    return this.http.get('/cards');
  }

  getCardById(id: string) {
    return this.http.get(`/details/${id}`);
  }
}
