import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CardsDashboardComponent } from './components/cards-dashboard/cards-dashboard.component';
import { CardDetailsComponent } from './components/card-details/card-details.component';
import { DetailsResolver } from './core/resolvers/details.resolver';

const routes: Routes = [
  {
    path: 'cards',
    component: CardsDashboardComponent
  },
  {
    path: 'cards/:id',
    component: CardDetailsComponent,
    resolve: { data: DetailsResolver }
  },
  {
    path: '**',
    redirectTo: 'cards'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabled'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
