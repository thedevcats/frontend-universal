import { Query, QueryEntity, HashMap } from '@datorama/akita';
import { Injectable } from '@angular/core';
import { CoreState, CoreStore } from './core.store';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CoreQuery extends QueryEntity<CoreState> {
  cards$: Observable<HashMap<any>> = this.selectAll();
  selectedCard$: Observable<any> = this.select(state => state.selectedCard);

  constructor(protected store: CoreStore) {
    super(store);
  }
}
