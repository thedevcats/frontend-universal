import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextTransformerPipe } from './pipes/text-transformer/text-transformer.pipe';



@NgModule({
  declarations: [TextTransformerPipe],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
