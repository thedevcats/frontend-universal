import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { CoreQuery } from '../../store/core.query';
import { CoreService } from '../../store/core.service';
import { HashMap } from '@datorama/akita';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  cards$: Observable<HashMap<any>> = this.coreQuery.cards$;
  selectedCard$: Observable<any> = this.coreQuery.selectedCard$;

  constructor(private coreQuery: CoreQuery, private coreService: CoreService) {}

  navigateToDetails(id: string) {
    this.coreService.getCardById(id);
  }
}
