import { Injectable } from '@angular/core';
import { CoreStore } from './core.store';
import { Router } from '@angular/router';
import { ApiService } from '../services/api/api.service';

@Injectable({ providedIn: 'root' })
export class CoreService {
  constructor(
    private coreStore: CoreStore,
    private apiService: ApiService,
    private router: Router
  ) {
    this.getAllCards();
  }

  getAllCards() {
    this.apiService.getAllCards().subscribe((cards: Array<any>) => {
      this.coreStore.set(cards);
    });
  }

  getCardById(id: string) {
    this.apiService.getCardById(id).subscribe((selectedCard: any) => {
      this.coreStore.update({
        selectedCard
      });
      this.router.navigate(['cards', id]);
    });
  }
}
