import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/core/services/app/app.service';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss']
})
export class CardDetailsComponent implements OnInit {
  selectedCard$: Observable<any>;

  constructor(private appService: AppService, private location: Location) {}

  ngOnInit(): void {
    if (this.appService.selectedCard$) {
      this.selectedCard$ = this.appService.selectedCard$;
    }
  }

  goBack() {
    this.location.back();
  }
}
