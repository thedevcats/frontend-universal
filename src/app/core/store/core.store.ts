import { Injectable } from '@angular/core';
import { StoreConfig, EntityState, EntityStore } from '@datorama/akita';

export interface CoreState extends EntityState<any, number> {
  selectedCard: any;
}

export function createInitialState(): CoreState {
  return {
    selectedCard: null
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'core', resettable: true })
export class CoreStore extends EntityStore<CoreState> {
  constructor() {
    super(createInitialState());
  }
}
